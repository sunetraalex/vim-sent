" Sent syntax file
" Language: Sent
" Maintainer: Alessandro Cutolo

" Quit when a syntax file was already loaded.
if exists("b:current_syntax")
    finish
endif

syntax match sentComment "^#.*$"
syntax match sentImage "^@.*$"

hi def link sentComment Comment
hi def link sentImage Statement

let b:current_syntax = 'sent'

