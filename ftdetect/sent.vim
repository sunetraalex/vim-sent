" Sent syntax file
" Language: Sent
" Maintainer: Alessandro Cutolo

autocmd BufNewFile,BufRead sent set filetype=sent
autocmd BufNewFile,BufRead sent.txt set filetype=sent
autocmd BufNewFile,BufRead *.sent set filetype=sent
autocmd BufNewFile,BufRead *.sent.txt set filetype=sent
