# VIM Sent syntax

An installable repo for the [sent](https://tools.suckless.org/sent/) syntax.

## Installation

With [VIM-Plug](https://github.com/junegunn/vim-plug):

```
Plug 'https://gitlab.com/sunetraalex/vim-sent.git'
```

then run `:PlugInstall`
